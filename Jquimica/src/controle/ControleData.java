package controle;
import modelo.Data;
public class ControleData {
    public Data preencher(int dia,int mes,int ano){
        Data data=new Data();
        data.setDia(dia);
        data.setMes(mes);
        data.setAno(ano);
        //converter pra int
        return data;
    }
    public boolean validar(Data data){
        boolean resultado;
        if (data.getMes()==2 || data.getMes()==4 || data.getMes()==6 || data.getMes()==9 || data.getMes()==11){
            if(data.getDia()>30){
                resultado=false;
            }else{
                resultado=true;
            }
        }else if(data.getMes()==2){
            if(data.getDia()>29){
                resultado=false;
            }else{
                resultado=true;
            }
        }else{
            if(data.getMes()<=12 && data.getMes()>=1){
                if(data.getDia()>31){
                    resultado=false;
                }else{
                    resultado=true;
                }
            }else{
                resultado=false;
            }
        }
        return resultado;
    }
}
