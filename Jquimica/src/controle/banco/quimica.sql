DROP SCHEMA IF EXISTS quimica;
CREATE SCHEMA IF NOT EXISTS quimica;
CREATE TABLE quimica.datas(
    id INT(4) PRIMARY KEY AUTO_INCREMENT,
    dia INT(2) NOT NULL,
    mes INT(2) NOT NULL,
    ano INT(4) NOT NULL
);
CREATE TABLE quimica.vidrarias(
    data INT(4),
    vidraria VARCHAR(50) PRIMARY KEY,
    quantidade INT(2),
    complemento VARCHAR(200),
    FOREIGN KEY (data) REFERENCES datas(id)
);
