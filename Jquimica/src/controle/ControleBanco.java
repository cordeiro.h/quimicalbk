package controle;
import modelo.Data;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import modelo.Vidraria;
public class ControleBanco {
    public Data selectUmaData(Vidraria vidro){
        Data data=new Data();
        try{
            Conexao con=new Conexao();
            PreparedStatement pse=con.getConexao().prepareStatement("SELECT * FROM datas WHERE id=?;");
            pse.setInt(1,vidro.getData());
            if(pse.execute()){
               ResultSet rse=pse.executeQuery();
               if(rse.next()){
                   data.setDia(rse.getInt("dia"));
                   data.setMes(rse.getInt("mes"));
                   data.setAno(rse.getInt("ano"));
                }
               con.fecharConexao();
            }
        }catch(Exception e){
            System.out.println("Erro: "+e.getMessage());
            data=null;
        }
        return data;
    }
    public boolean updateData(Data data){
        boolean resultado;
        try{
            Conexao con=new Conexao();
            PreparedStatement pse=con.getConexao().prepareStatement("UPDATE datas SET dia=?,mes=?,ano=? WHERE id=?;");
            pse.setInt(1,data.getDia());
            pse.setInt(2,data.getMes());
            pse.setInt(3,data.getAno());
            pse.setInt(4,data.getId());
            if(!pse.execute()){
                con.fecharConexao();
                resultado=true;
            }else{
                con.fecharConexao();
                resultado=false;
            }
        }catch(Exception e){
            System.out.println("Erro: "+e.getMessage());
            resultado=false;
        }
        return resultado;
    }
    public boolean updateVidraria(Vidraria vidro){
        boolean resultado;
        try{
            Conexao con=new Conexao();
            PreparedStatement pse=con.getConexao().prepareStatement("UPDATE vidrarias SET vidraria=?,quantidade=?,complemento=? WHERE vidraria=?;");
            pse.setString(1,vidro.getNome());
            pse.setInt(2,vidro.getQuant());
            pse.setString(3,vidro.getComplemento());
            pse.setString(4,vidro.getNome());
            if(!pse.execute()){
                con.fecharConexao();
                resultado=true;
            }else{
                con.fecharConexao();
                resultado=false;
            }
        }catch(Exception e){
            System.out.println("Erro: "+e.getMessage());
            resultado=false;
        }
        return resultado;
    }
    public Vidraria selectUmVidro(Vidraria vidraria){
        Vidraria vidro=new Vidraria();
        try{
            Conexao con=new Conexao();
            PreparedStatement pse=con.getConexao().prepareStatement("SELECT * FROM vidrarias WHERE vidraria=?;");
            pse.setString(1,vidraria.getNome());
            if(pse.execute()){
               ResultSet rse=pse.executeQuery();
               if(rse.next()){
                   vidro.setNome(rse.getString("vidraria"));
                   vidro.setQuant(rse.getInt("quantidade"));
                   vidro.setData(rse.getInt("data"));
                   vidro.setComplemento(rse.getString("complemento"));
               }
               con.fecharConexao();
            }
        }catch(Exception e){
            System.out.println("Erro: "+e.getMessage());
            vidro=null;
        }
        return vidro;
    }
    public boolean inserirData(Data data){
        boolean resultado;
        try{
            Conexao con=new Conexao();
            PreparedStatement pse=con.getConexao().prepareStatement("INSERT INTO datas(dia,mes,ano) VALUES(?,?,?);");
            pse.setInt(1,data.getDia());
            pse.setInt(2,data.getMes());
            pse.setInt(3, data.getAno());
            if(!pse.execute()){
                con.fecharConexao();
                resultado=true;
            }else{
                con.fecharConexao();
                resultado=false;
            }
        }catch(Exception e){
            System.out.println("Erro: "+ e.getMessage());
            resultado=false;
        }
        return resultado;
    }
    public boolean inserirVidro(Vidraria vidro){
        boolean resultado;
        try{    
            Conexao con=new Conexao();
            PreparedStatement pse=con.getConexao().prepareStatement("INSERT INTO vidrarias(vidraria,quantidade,complemento,data) VALUES(?,?,?,?);");
            pse.setString(1,vidro.getNome());
            pse.setInt(2,vidro.getQuant());
            pse.setString(3,vidro.getComplemento());
            pse.setInt(4, vidro.getData());
            if(!pse.execute()){
                con.fecharConexao();
                resultado=true;
            }else{
                con.fecharConexao();
                resultado=false;
            }
        }catch(Exception e){
            System.out.println("Erro geral: "+e.getMessage());
            resultado=false;
        }
        return resultado;
    }
    public int selectIdData(){
        int resultado=0;
        try{
            Conexao con=new Conexao();
            PreparedStatement pse=con.getConexao().prepareStatement("SELECT * FROM datas;");
            if(pse.execute()){
                ResultSet rse=pse.executeQuery();
                if(rse!=null){
                    while(rse.next()){
                        resultado=rse.getInt("id");
                    }
                }
                con.fecharConexao();
            }
        }catch(Exception e){
            System.out.println("Erro geral: "+e.getMessage());
            return resultado;
        }
        return resultado;
    }
}
