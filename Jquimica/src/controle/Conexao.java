package controle;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
public class Conexao {
    private Connection conexao;
    public void setConexao(Connection conexao){
        this.conexao=conexao;
    }
    public Connection getConexao(){
        return this.conexao;
    }
    public Connection abrirConexao(){
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            String banco="quimica";
            String url="jdbc:mysql://localhost/"+banco;
            String user="root";
            String senha="12081973";
            this.setConexao(DriverManager.getConnection(url,user,senha));
            if(this.getConexao()!=null){
                return this.getConexao();
            }else{
                return null;
            }
        }catch(ClassNotFoundException e){
            System.out.println("Erro na biblioteca: "+e.getMessage());
            return null;
        }catch(SQLException e){
            System.out.println("Erro no banco: "+e.getMessage());
            return null;
        }catch(Exception e){
            System.out.println("Erro geral: "+e.getMessage());
            return null;
        }
    }
    public Conexao(){
        abrirConexao();
    }
    public void fecharConexao(){
        try{
            this.getConexao().close();
        }catch(SQLException e){
            System.out.println("Erro no banco: "+e.getMessage());
        }
    }
}
