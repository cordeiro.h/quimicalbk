package modelo;
public class Vidraria {
    private String nome;
    private int data;
    private int quant;
    private String complemento;
    public void setNome(String nome){
        this.nome=nome;
    }
    public String getNome(){
        return this.nome;
    }
    public void setData(int data){
        this.data=data;
    }
    public int getData(){
        return this.data;
    }
    public void setQuant(int quant){
        this.quant=quant;
    }
    public int getQuant(){
        return this.quant;
    }
    public void setComplemento(String complemento){
        this.complemento=complemento;
    }
    public String getComplemento(){
        return this.complemento;
    }
}
