package modelo;
public class Data {
    private int id;
    private int dia;
    private int mes;
    private int ano;
    public void setId(int id){
        this.id=id;
    }
    public int getId(){
        return this.id;
    }
    public void setDia(int dia){
        this.dia=dia;
    }
    public int getDia(){
        return this.dia;
    }
    public void setMes(int mes){
        this.mes=mes;
    }
    public int getMes(){
        return this.mes;
    }
    public void setAno(int ano){
        this.ano=ano;
    }
    public int getAno(){
        return this.ano;
    }
}
