*Trabalho do curso de Técnico em Informática*

 <ol>
  <li>Bruno Lima</li>
  <li>Kauan Cordeiro</li>
  <li>Maria Letícia</li>
</ol>
 
<p>
    A aplicação tem o objetivo de ajudar na contagem de vidrarias do laboratório
de química da escola, pois são itens não duráveis, a aplicação facilita a 
consulta de vidrarias em determinadas datas, e assim, deixando saber quantas
vidrarias quebraram, não prestam mais, ou quais precisam e estão em falta. </p>
<p>
Aplicação feita em java com banco de dados mysql</p>